package me.tijme.data;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

public class PlayerEconomy {
	UUID uuid;
	int coins;
	int exp;

	// Constructor
	public PlayerEconomy(UUID uuid, int coins, int exp) {
		this.coins = coins;
		this.uuid = uuid;
		this.exp = exp;
	}

	// Save to database
	public void saveToDataBase() {

	}

	// Get from database
	public void getFromDataBase() {

	}

	public int getCoins() {
		return coins;
	}

	public int getExp() {
		return exp;
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setCoins(int coins) {
		this.coins = coins;
		saveToDataBase();
	}

	public void setExp(int exp) {
		this.exp = exp;
		saveToDataBase();
	}
}
