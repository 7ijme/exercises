package me.tijme.data;

import me.tijme.Main;

import java.util.UUID;

public class TpaData {

	Main plugin;
	UUID playerToTeleport;
	UUID playerTeleportTo;

	// Constructor
	public TpaData(UUID playerToTeleport, UUID playerTeleportTo, Main plugin) {
		this.playerToTeleport = playerToTeleport;
		this.playerTeleportTo = playerTeleportTo;
		this.plugin = plugin;
	}

}
