package me.tijme.data;

import me.tijme.Main;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class WorldProtectionEvent implements Listener {

	Main plugin;

	public WorldProtectionEvent(Main plugin) {
		this.plugin = plugin;
	}


	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		// Check if it was a jump on soil
		if (e.getAction() == Action.PHYSICAL && e.getClickedBlock().getType() == Material.SOIL) {
			e.setCancelled(true);

			Player player = e.getPlayer();
			World world = player.getWorld();

			// Loop through blocks
			Block block = null;
			loop: for (int i = -2; i < 2; i++) {
				for (int j = -2; j < 2; j++) {
					Block blockAboveHighestBlock = world.getHighestBlockAt((int) (player.getLocation().getX() + i), (int) (player.getLocation().getZ() + j));
					Block currentBlock = player.getWorld().getBlockAt(blockAboveHighestBlock.getX(), blockAboveHighestBlock.getY()-1, blockAboveHighestBlock.getZ());
					if (currentBlock.getType() != Material.SOIL && currentBlock.getType() != Material.STATIONARY_LAVA && currentBlock.getType() != Material.STATIONARY_WATER && currentBlock.getType() != Material.AIR) {
						block = blockAboveHighestBlock;
						break loop;
					}
				}
			}

			// If there's no block
			if (block == null) {
				player.sendMessage(plugin.utils.colorChat("&6There is not space to teleport"));
				return;
			}

			// Teleport and message
			player.teleport(block.getLocation());
			player.sendMessage(plugin.utils.colorChat("&6Hey! Don't try to remove soil."));

		}
	}
}
