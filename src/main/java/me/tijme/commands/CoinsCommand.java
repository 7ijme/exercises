package me.tijme.commands;

import me.tijme.Main;
import me.tijme.data.PlayerEconomy;
import me.tijme.utils.Utils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;
import java.util.UUID;

public class CoinsCommand implements CommandExecutor {

	Main plugin;
	Utils utils;
	HashMap<UUID, PlayerEconomy> playerEconomys;


	// Constructor
	public CoinsCommand(Main plugin) {
		this.plugin = plugin;
		this.playerEconomys = plugin.playerEconomys;
		this.utils = plugin.utils;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
		// If console executes the command
		if (!(sender instanceof Player)) {
			plugin.getLogger().info("Only players can execute this command.");
			return true;
		}

		// Get coins
		Player player = (Player) sender;
		int coins = playerEconomys.get(player.getUniqueId()).getCoins();
		player.sendMessage(utils.colorChat("&4Je hebt &6" + coins + " &4coins!"));
		return true;
	}
}
