package me.tijme.commands;

import me.tijme.Main;
import me.tijme.data.PlayerEconomy;
import me.tijme.utils.Utils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.Arrays;
import java.util.HashMap;
import java.util.UUID;

public class SetCoinsCommand implements CommandExecutor {
	Main plugin;
	Utils utils;
	HashMap<UUID, PlayerEconomy> playerEconomys;

	// Constructor
	public SetCoinsCommand(Main plugin) {
		this.plugin = plugin;
		this.playerEconomys = plugin.playerEconomys;
		this.utils = plugin.utils;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
		// If console executes the command
		if (!(sender instanceof Player)) {
			plugin.getLogger().info("Only players can execute this command.");
			return true;
		}


		Player player = (Player) sender;
		PlayerEconomy playerEconomy = playerEconomys.get(player.getUniqueId());

		// Check if a valid arg is passed through
		if (!(strings.length > 0) || !utils.isNumber(strings[0])) return false;

		// Set coins
		playerEconomy.setCoins(Integer.parseInt(strings[0]));
		player.sendMessage(utils.colorChat("&4Je hebt nu &6" + strings[0] + " &4coins!"));
		return true;
	}
}
