package me.tijme.commands;

import me.tijme.Main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TpaDenyCommand implements CommandExecutor {

	Main plugin;

	public TpaDenyCommand(Main plugin) {
		this.plugin = plugin;
	}


	@Override
	public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
		// If console executes the command
		if (!(sender instanceof Player)) {
			plugin.getLogger().info("Only players can execute this command.");
			return true;
		}

		Player player = (Player) sender;

		// Check if there is a player
		if (strings.length != 1 || plugin.getServer().getOnlinePlayers().stream().filter(p -> p.getName().equals(strings[0])).toArray().length != 1) {
			return false;
		}

		// Get the player
		Player tpaFromPlayer = null;
		for (Player p : plugin.getServer().getOnlinePlayers()) {
			plugin.getLogger().info(player.getName());
			if (strings[0].equals(p.getName())) {
				tpaFromPlayer = p;
				break;
			}
		}

		// Check whether he was tpaed
		assert tpaFromPlayer != null;
		if (!plugin.tpaData.containsKey(tpaFromPlayer.getUniqueId().toString() + player.getUniqueId().toString())) {
			player.sendMessage(plugin.utils.colorChat("&6" + tpaFromPlayer.getName() + " didn't ask you if you could teleport to them."));
			return true;
		}

		// Send messages and remove tpa
		plugin.tpaData.remove(tpaFromPlayer.getUniqueId().toString() + player.getUniqueId().toString());
		tpaFromPlayer.sendMessage(plugin.utils.colorChat("&6" + player.getName() + " denied your tpa."));
		tpaFromPlayer.sendMessage(plugin.utils.colorChat("&6You denied " + tpaFromPlayer.getName() + "'s tpa."));


		return true;
	}
}
