package me.tijme.commands;

import me.tijme.Main;
import me.tijme.data.PlayerEconomy;
import me.tijme.utils.Utils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

public class ExpCommand implements CommandExecutor {
	Main plugin;
	Utils utils;
	HashMap<UUID, PlayerEconomy> playerEconomys;


	// Constructor
	public ExpCommand(Main plugin) {
		this.plugin = plugin;
		this.playerEconomys = plugin.playerEconomys;
		this.utils = plugin.utils;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
		// If console executes the command
		if (!(sender instanceof Player)) {
			plugin.getLogger().info("Only players can execute this command.");
			return true;
		}

		// Get exp
		Player player = (Player) sender;
		int exp = playerEconomys.get(player.getUniqueId()).getExp();
		player.sendMessage(utils.colorChat("&4Je hebt &6" + exp + " &4exp!"));
		return true;
	}
}
