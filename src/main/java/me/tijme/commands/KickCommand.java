package me.tijme.commands;

import me.tijme.Main;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class KickCommand implements CommandExecutor {
	Main plugin;

	public KickCommand(Main plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
		// If console executes the command
		if (!(sender instanceof Player)) {
			plugin.getLogger().info("Only players can execute this command.");
			return true;
		}

		Player player = (Player) sender;
		// Check if there is a player
		if (strings.length != 1 || plugin.getServer().getOnlinePlayers().stream().filter(p -> p.getName().equals(strings[0])).toArray().length != 1) {
			return false;
		}

		// Create inventory
		Inventory inv = plugin.getServer().createInventory(player, 27, plugin.utils.colorChat("&4Kick " + strings[0]));

		ItemStack greenWool = new ItemStack(Material.WOOL, 1, (byte) 5);
		ItemMeta greenWoolMeta = greenWool.getItemMeta();
		greenWoolMeta.setDisplayName(plugin.utils.colorChat("&aSpamming"));
		greenWool.setItemMeta(greenWoolMeta);

		ItemStack orangeWool = new ItemStack(Material.WOOL, 1, (byte) 1);
		ItemMeta orangeWoolMeta = orangeWool.getItemMeta();
		orangeWoolMeta.setDisplayName(plugin.utils.colorChat("&6Swearing"));
		orangeWool.setItemMeta(orangeWoolMeta);

		ItemStack redWool = new ItemStack(Material.WOOL, 1, (byte) 14);
		ItemMeta redWoolMeta = redWool.getItemMeta();
		redWoolMeta.setDisplayName(plugin.utils.colorChat("&4Cheating"));
		redWool.setItemMeta(redWoolMeta);

		inv.setItem(11, greenWool);
		inv.setItem(13, orangeWool);
		inv.setItem(15, redWool);
		player.openInventory(inv);
		return true;
	}
}
