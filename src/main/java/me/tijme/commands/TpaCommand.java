package me.tijme.commands;

import me.tijme.Main;
import me.tijme.data.TpaData;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TpaCommand implements CommandExecutor {

	Main plugin;

	public TpaCommand(Main plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
		// If console executes the command
		if (!(sender instanceof Player)) {
			plugin.getLogger().info("Only players can execute this command.");
			return true;
		}

		Player player = (Player) sender;

		// Check if there is a player
		if (strings.length != 1 || plugin.getServer().getOnlinePlayers().stream().filter(p -> p.getName().equals(strings[0])).toArray().length != 1) {
			return false;
		}

		// Get the player
		Player playerToTpa = null;
		for (Player p : plugin.getServer().getOnlinePlayers()) {
			plugin.getLogger().info(player.getName());
			if (strings[0].equals(p.getName())) {
				playerToTpa = p;
				break;
			}
		}

		// Storing data
		assert playerToTpa != null;
		plugin.tpaData.put(player.getUniqueId().toString() + playerToTpa.getUniqueId().toString(), new TpaData(player.getUniqueId(), playerToTpa.getUniqueId(), plugin));

		// Sending players messages
		player.sendMessage(plugin.utils.colorChat("&6Asking " + playerToTpa.getName() + " to teleport to them."));
		playerToTpa.sendMessage(plugin.utils.colorChat("&6" + player.getName() + " asked you to teleport to you.\nUse &4/tpaaccept " + player.getName() + "&6to let them teleport to you."));

		return true;
	}
}
