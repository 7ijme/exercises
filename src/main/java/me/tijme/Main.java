package me.tijme;

import me.tijme.commands.*;
import me.tijme.data.PlayerEconomy;
import me.tijme.data.TpaData;
import me.tijme.data.WorldProtectionEvent;
import me.tijme.listeners.AsyncChatEvent;
import me.tijme.listeners.JoinEvent;
import me.tijme.listeners.PlayerInventoryClickEvent;
import me.tijme.utils.Utils;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.UUID;

public class Main extends JavaPlugin {

	public HashMap<UUID, PlayerEconomy> playerEconomys;
	public HashMap<String, TpaData> tpaData;
	public Utils utils;

	@Override
	public void onEnable() {
		this.getLogger().info("Tijme's plugin started working.");

		playerEconomys = new HashMap<>();
		tpaData = new HashMap<>();
		utils = new Utils();

		saveDefaultConfig();

		loadEconomy();
		loadEvents();
		loadCommands();
	}

	private void loadEvents() {
		// Load the events
		getServer().getPluginManager().registerEvents(new AsyncChatEvent(this), this);
		getServer().getPluginManager().registerEvents(new JoinEvent(this, playerEconomys), this);
		getServer().getPluginManager().registerEvents(new PlayerInventoryClickEvent(this), this);
		getServer().getPluginManager().registerEvents(new WorldProtectionEvent(this), this);
	}

	private void loadCommands() {
		// Load the commands
		getCommand("coins").setExecutor(new CoinsCommand(this));
		getCommand("exp").setExecutor(new ExpCommand(this));
		getCommand("setcoins").setExecutor(new SetCoinsCommand(this));
		getCommand("setexp").setExecutor(new SetExpCommand(this));
		getCommand("kick").setExecutor(new KickCommand(this));
		getCommand("tpa").setExecutor(new TpaCommand(this));
		getCommand("tpaaccept").setExecutor(new TpaAcceptCommand(this));
		getCommand("tpadeny").setExecutor(new TpaDenyCommand(this));
	}

	private void loadEconomy() {
		for (Player player : getServer().getOnlinePlayers()) {
			PlayerEconomy playerEconomy = new PlayerEconomy(player.getUniqueId(), 0, 0);
			playerEconomy.getFromDataBase();

			playerEconomys.put(player.getUniqueId(), playerEconomy);
		}
	}

}
