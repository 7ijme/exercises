package me.tijme.listeners;

import me.tijme.Main;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class PlayerInventoryClickEvent implements Listener {
	Main plugin;

	// Constructor
	public PlayerInventoryClickEvent(Main plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	@Deprecated
	public void onInventoryClick(InventoryClickEvent e) {
		// Check if it's the kick inventory
		if (e.getInventory().getName().contains("Kick")) {
			e.setCancelled(true);

			if (!(e.getCurrentItem().getType() == Material.WOOL)) return;

			// Get the name that's inside the inventory name
			String playerName = e.getInventory().getName().substring(7);

			// Cases
			switch (e.getCurrentItem().getData().getData()) {
				case 5:
					kickPlayer((Player) e.getWhoClicked(), playerName, plugin.utils.colorChat("&aYou were spamming.\nPlease don't do it again."));
					break;
				case 1:
					kickPlayer((Player) e.getWhoClicked(), playerName, plugin.utils.colorChat("&6You were swearing.\nPlease don't do it again."));
					break;
				case 14:
					kickPlayer((Player) e.getWhoClicked(), playerName, plugin.utils.colorChat("&4You were cheating.\nPlease don't do it again."));
					break;
			}
		}
	}

	public void kickPlayer(Player sender, String playerName, String reason) {
		// Get the player
		Player playerToKick = null;
		for (Player player : plugin.getServer().getOnlinePlayers()) {
			plugin.getLogger().info(player.getName());
			if (playerName.equals(player.getName())) {
				playerToKick = player;
				break;
			}
		}

		// If the player is offline
		if (playerToKick == null) {
			sender.sendMessage(plugin.utils.colorChat("&4This player already left."));
			return;
		}

		// Kick
		playerToKick.kickPlayer(reason);
		sender.sendMessage(plugin.utils.colorChat("&6" + playerName + " &7has been kicked for &6" + reason));
	}
}
