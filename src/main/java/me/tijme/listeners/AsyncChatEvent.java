package me.tijme.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.Plugin;

import java.util.List;

public class AsyncChatEvent implements Listener {

	Plugin plugin;

	public AsyncChatEvent(Plugin plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onChat(AsyncPlayerChatEvent e) {
		// Chat Filter

		// Get swearwords
		List<String> badSwearwords = plugin.getConfig().getStringList("swearwords.bad");
		List<String> okaySwearwords = plugin.getConfig().getStringList("swearwords.okay");



		// Strong filter

		// Change the message its vowels, numbers and every other character (excluding consonants) to a *.
		String newMessage = e.getMessage().replaceAll("\\W|[0-9]|[aeuio]", "");
		for (String word : badSwearwords) {
			// Change the words its vowels, numbers and every other character (excluding consonants) to a *.
			String newWord = word.replaceAll("\\W|[0-9]|[aeuio]", "");

			// Check whether the word contains the message
			if (newMessage.contains(newWord)) {
				e.setCancelled(true);
				plugin.getLogger().info(e.getPlayer().getName() + " heeft gescholden met " + word + " in deze zin: " + e.getMessage());
				for (Player player : plugin.getServer().getOnlinePlayers()) {
					if (player.hasPermission("rank.helper")) {
						player.sendMessage(e.getPlayer().getName() + " heeft gescholden met " + word + " in deze zin: " + e.getMessage());
					}
				}
				return;
			}
		}

		// Soft filter
		for (String word : okaySwearwords) {
			// Check whether the word contains the message
			if (e.getMessage().contains(word)) {
				e.setCancelled(true);
				plugin.getLogger().info(e.getPlayer().getName() + " heeft gescholden met " + word + " in deze zin: " + e.getMessage());
				for (Player player : plugin.getServer().getOnlinePlayers()) {
					if (player.hasPermission("rank.helper")) {
						player.sendMessage(e.getPlayer().getName() + " heeft gescholden met " + word + " in deze zin: " + e.getMessage());
					}
				}
				return;
			}
		}
	}
}
