package me.tijme.listeners;

import me.tijme.Main;
import me.tijme.data.PlayerEconomy;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.HashMap;
import java.util.UUID;

public class JoinEvent implements Listener {

	Main plugin;
	HashMap<UUID, PlayerEconomy> playerEconomys;

	public JoinEvent(Main plugin, HashMap<UUID, PlayerEconomy> playerEconomys) {
		this.plugin = plugin;
		this.playerEconomys = plugin.playerEconomys;
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		// Create playerEconomy
		if (!playerEconomys.containsKey(e.getPlayer().getUniqueId())) {
			PlayerEconomy playerEconomy = new PlayerEconomy(e.getPlayer().getUniqueId(), 0, 0);
			playerEconomy.getFromDataBase();

			playerEconomys.put((e.getPlayer().getUniqueId()), playerEconomy);
		}
	}
}
