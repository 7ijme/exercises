package me.tijme.utils;

import org.bukkit.ChatColor;

public class Utils {
	public String colorChat(String s) {
		return ChatColor.translateAlternateColorCodes('&', s);
	}

	public boolean isNumber(String s) {
		try {
			@SuppressWarnings("unused")
			int x = Integer.parseInt(s);
			return true; //String is an Integer
		} catch (NumberFormatException e) {
			return false; //String is not an Integer
		}
	}
}
